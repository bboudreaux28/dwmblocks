//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"",           "sb-music",                                                                       0,    3},
	{"      MEM:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	                 30,	0},
	/*{" ",          "sb-torrent",                                                                     20,    5},*/
	/*{"     ",               "check-weather",                                                       300,    0},*/
	{" ",          "sb-weather",                                                                   2700,    4},
	{"",           "mail",                                                                            0,    1},
	{" ",          "sb-volume",       		                                                  0,    2},
	{"      ",          "date '+%b %d (%a)'",   		                                         60,    0},
	{"     ",                       "date '+%I:%M%p'",   		                                 60,    0},
	{"^c#FFDF00^",                  "sed 's/.*/    /' /sys/class/net/t*/operstate 2>/dev/null",     40,    0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "   ";
static unsigned int delimLen = 5;
